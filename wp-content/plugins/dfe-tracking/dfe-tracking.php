<?php
/**
 * Plugin Name: DFE Tracking
 * Version: 0.1
 * Description: Paste this shortcode in yout page ([dfe-tracking])
 * Text Domain: dfe-tracking
 * Author: Digilite
 * Author URI: https://digilite.ca
 */


register_activation_hook(__FILE__, 'dfe_activation');
add_action('admin_init', 'dfe_redirect');

function dfe_activation() {
    add_option('dfe_activation_redirect', true);
}

function dfe_redirect() {
    if (get_option('dfe_activation_redirect', false)) {
        delete_option('dfe_activation_redirect');
        if(!isset($_GET['activate-multi']))
        {
            wp_redirect("admin.php?page=dfe-settings");
        }
    }
}

add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts');
function callback_for_setting_up_scripts() {
    wp_register_style( 'namespace', get_site_url().'/wp-content/plugins/dfe-tracking/css/dfe-tracking.css' );
    wp_enqueue_style( 'namespace' );
    wp_enqueue_script( 'namespaceformyscript', get_site_url().'/wp-content/plugins/dfe-tracking/js/dfe-tracking.js', array( 'jquery' ) );
}


function dfe_tracking($atts) { 
	$buttoncolor = get_option('df_button_color');
	$buttonbg = get_option('df_bg_color');
	$fieldcolor = get_option('df_field_color');
	$fieldbg = get_option('df_field_bg');
	$trackingtitle = get_option('df_tracking_title');
	$df_notice = get_option('df_notice');
	$df_field_border_color = get_option('df_field_border_color');
	$df_button_border_color = get_option('df_button_border_color');
    return '<div class="dfe-tracking-field">
              <h3 class="dfe-title">'. $trackingtitle .'</h3>
              <input style="background-color: '. $fieldbg .'; color: '. $fieldcolor .'; border: 1px solid '. $df_field_border_color .'" class="dfe-track-input" type="text" name="dfe-track" value="" placeholder="Enter consignment tracking number">
              <a class="dfe-submit" style="background-color: '. $buttonbg .'; color: '. $buttoncolor .'; border: 1px solid '. $df_button_border_color .'">Track</a>
			  <p class="dfe_notice">'. $df_notice .'</p>
           </div>';
}

add_shortcode('dfe-tracking', 'dfe_tracking');


function add_theme_menu_item()
{
	add_submenu_page("woocommerce", "DFE tracking", "DFE tracking", "manage_options", "dfe-settings", "dfe_plugin_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");


function dfe_plugin_settings_page()
{
    ?>
	    <div class="wrap">
	    <form method="post" action="options.php">
	        <?php
			
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>
			<p>Choose your styles and save settings</p>
			<p>Copy this shortcode in your page | [dfe-tracking]</p>        
	    </form>
		</div>
	<?php
}

include 'dfe-tracking-settings.php';