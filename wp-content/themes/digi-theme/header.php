<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="notification">
  <div class="container">
    <div class="note-block">
      <p><i data-feather="bell"></i><span>Register now and receive up to $50 off your first purchase!</span></p>
      <span class="close-note">X</span>
    </div>
  </div>
</div><!-- notification -->

<div class="specification">
<div class="container">
  <div class="spec-parent">
    <?php
    if( have_rows("specifications", "option") ):
      while( have_rows("specifications", "option") ) : the_row();
        $icon = get_sub_field("sp_icon", "option");
        $text = get_sub_field("sp_description", "option"); ?>
        <div class="spec-item">
          <div class="bg-styles" style="background-image: url(<?= $icon ?>);"></div>
          <p class="spec-item-description"><?= $text ?></p>
        </div>
      <?php endwhile;
    endif; ?>
  </div>
</div>
</div><!-- specification -->

<header itemscope itemtype="http://schema.org/WPHeader">
<div class="gray-bg">
  <div class="main-header container flex-container">
    <div class="mobile-menu">
    <div class="header-items">
      <div class="main-nav flex-container">
          <nav class="navbar-expand-lg navbar-light" itemscope itemtype="http://schema.org/SiteNavigationElement">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="burger-one"></span>
              <span class="burger-two"></span>
              <span class="burger-three"></span>
          </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <?php
          wp_nav_menu([
            'theme_location' => 'primary-menu',
            'menu_class' => 'main-menu flex-container',
            'container' => '',
          ]); ?>
        </div>
        </nav>
      </div>
    </div>
    </div><!-- mobile-menu -->
    <div itemscope itemtype="http://schema.org/Organization" id="logo">
      <a itemprop="url" href="<?php echo bloginfo('url') ?>">
        <img class="site-logo" itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo.svg">
      </a>
    </div>
    <div class="search">
      <?php get_search_form(); ?>
    </div>
    <div class="top-right-icons flex-container relative">
      <a href="<?php echo site_url();?>/my-account" class="login-icon">
        <i data-feather="user"></i>
      </a>
      <div class="cart">
        <div class="cart-bag"></div>
        <span class="cart-icon">
          <i data-feather="shopping-cart"></i>
        </span>
        <p class="cart-contents">0</p>
      </div>
    </div>
  </div><!-- Main Header -->
  <span id="menu-opener"><i data-feather="chevron-down"></i></span>
</div>
<div class="desktop-menu the-menu">
  <div class="container header-items">
    <div class="main-nav flex-container">
      <nav class="navbar-expand-lg navbar-light" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <?php
        wp_nav_menu([
          'theme_location' => 'primary-menu',
          'menu_class' => 'main-menu flex-container',
          'container' => '',
        ]); ?>
      </nav>
      <div class="header-phone">
        <a href="tel:<?php the_field('phone_number', 'options'); ?>" class="header-phone-number"><i data-feather="phone"></i><span><?php the_field('phone_number', 'options'); ?></span></a>
      </div>
    </div>
  </div>
</div><!-- desktop-menu -->
</header>

