		<?php
			$phone = get_field("phone_number", "options");
			$address = get_field("address", "options");
		?>
		<footer itemscope itemtype="http://schema.org/WPFooter">
			<div class="container">
				<div class="footer-main">
					<div class="row">
						<div class="footer-logo col-md-3 col-6">
							<?php
							$logo = get_field('footer_logo', 'options');
							if( !empty( $logo ) ): ?>
								<a href="<?= bloginfo('url') ?>"><img src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>" /></a>
							<?php endif; ?>
						</div>
						<div class="footer-menu footer-menu-section-1 col-md-3 col-6">
							<?php wp_nav_menu([
								'theme_location' => 'footer-menu-1',
								'menu_class' => 'ft-menu footer-menu-1',
								'container' => '',
							]); ?>
						</div>
						<div class="footer-menu footer-menu-section-2 col-md-3 col-6">
							<?php wp_nav_menu([
								'theme_location' => 'footer-menu-2',
								'menu_class' => 'ft-menu footer-menu-2',
								'container' => '',
							]); ?>
						</div>
						<div class="footer-contact col-md-3 col-6">
            <a href="tel:<?php the_field('phone_number', 'options'); ?>" class="ft-contact-phone"><i data-feather="phone"></i><span><?= $phone; ?></span></a>
							<div class="flex-container">
							<i data-feather="map-pin"></i><p class="ft-contact-address"><span><?= $address; ?></span></p>
							</div>
              <div class="d-lg-flex subscribe social-links">
                <a href="#"><i class="fab fa-facebook-f"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
              </div>
						</div>
					</div>
				</div>
			</div>
			<div class="bottom-bar">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p class="copy-info">&copy; <?php echo get_bloginfo("name") . " " . date("Y"); ?>.All rights reserved. Made by <a href="https://digilite.ca">Digilite</a></p>
						</div>
						<div class="col-md-6">
							<?php wp_nav_menu([
								'theme_location' => 'footer-menu-3',
								'menu_class' => 'ft-menu footer-menu-3 flex-container',
								'container' => '',
							]); ?>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>
