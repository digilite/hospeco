<?php get_header(); ?>
<?php get_template_part("templates/hero"); ?>

<div class="container">
<div class="breadcrumbs">
  <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
</div>
<?php	if (have_posts()) :
		while (have_posts()) :
			the_post(); ?>
      <article class="bg-text-over">
        <div class="single-hero">
            <div class="the-bg bg-styles" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
            <div class="single-hero-content">
              <p>Posted on <?php the_date( "d/m/Y" ); ?></p>
              <h2 class="h3"><?php the_title() ?></h2>
            </div><!-- the-bg-content -->
        </div>
      <div class="single-content">
        <?php the_content(); ?>
      </div>
      </article>
      <?php
		endwhile;
	endif; ?>
</div>

<div class="container relative-parent more-blogs">
<h3 class="block-title">Read More Blogs</h3>
<a href="<?php echo site_url('blog')?>" class="view-all-cat">View all</a>
<?php $posts_query = array(
    "post_type" => "post",
    "orderby" => "rand"
  );
$posts = new WP_Query( $posts_query );
if ($posts->have_posts()) : ?>
<div class="owl-carousel owl-theme" id="posts_carousel">
<?php
while ($posts->have_posts()) :
    $posts->the_post(); ?>
    <div class="blog-post-box">
      <div class="blog-thumb">
        <a class="img-link" href="<?php the_permalink(); ?>">
        <?php
          if ( has_post_thumbnail() ) { ?>
            <img src="<?php the_post_thumbnail_url() ?>" class="img-fluid blog-img" alt="blog image">
          <?php
          } else { ?>
            <img src="<?= site_url('/wp-content/themes/digi-theme/img/logo.svg') ?>" class="img-fluid blog-img blog-back-image" alt="blog image">
        <?php } ?>
        </a>
      </div>
      <h2 class="h5"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
    </div>
<?php
endwhile; ?>
</div>
<?php
endif;
wp_reset_postdata();
?>
</div>


<?php get_footer(); ?>
