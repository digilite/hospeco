<?php get_header();
            $paged1 = isset( $_GET['paged1'] ) ? (int) $_GET['paged1'] : 1;
            $paged2 = isset( $_GET['paged2'] ) ? (int) $_GET['paged2'] : 1;

			$s_service =get_search_query();

			$args = array(
				'posts_per_page'   => '3',
				'post_type'        => 'service',
				'order' => 'ASC',
				'paged'          => $paged1,
				's' => $s_service,
			);

			$loop = new WP_Query( $args );
			$count_services = $loop->post_count;

			$s_product =get_search_query();

				$args_p = array(
					'posts_per_page'   => -1,
					'post_type'        => 'product',
					'order' => 'ASC',
					's' => $s_product,
				);

		  $loop_p = new WP_Query( $args_p );
			$count_product = $loop_p->post_count;

			$s_blog =get_search_query();

			$args_b = array(
				'posts_per_page'   => '3',
				'post_type'        => 'post',
				'paged'          => $paged2,
				'order' => 'ASC',
				's' => $s_blog,
			);
			$loop_b = new WP_Query( $args_b );
			$count_blog = $loop_b->post_count;
 ?>

<div class="container relative-parent">
      <div class="search-title">
        <h3 class="block-title"><?php printf( __( 'Search Results for "%s', 'shape' ), '<span>' . get_search_query() . '"</span>' ); ?></h3>
      </div><!-- search-title -->
			<ul class="nav nav-tabs" id="searchtab" role="tablist">
        <li class="nav-item waves-effect waves-light">
          <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#products" role="tab" aria-controls="products" aria-selected="false">Products(<?php echo $count_product ?>)</a>
        </li>
        <li class="nav-item waves-effect waves-light">
          <a class="nav-link " id="blog-tab" data-toggle="tab" href="#blog" role="tab" aria-controls="blog" aria-selected="true">Blog(<?php echo $count_blog ?>)</a>
        </li>
      </ul>
			<div class="tab-content" id="myTabContent">
        <div class="tab-pane fade active show" id="products" role="tabpanel" aria-labelledby="products-tab">
				<?php

				if ($loop_p->have_posts()) :
				while ( $loop_p->have_posts() ) : $loop_p->the_post();
				 $ids[]=get_the_ID();

				endwhile;
				endif;
				// wp_reset_postdata();
				?><?php

					if($ids){
						$format_ids = implode( ",",$ids );
						echo do_shortcode("[products ids=".$format_ids." limit='12' paginate='1' ]");
					}

					?>

				</div>
        <div class="tab-pane fade " id="blog" role="tabpanel" aria-labelledby="blog-tab">
					<div class="row">
						<?php

						if ($loop_b->have_posts()) :
						while ( $loop_b->have_posts() ) : $loop_b->the_post();

							?>
							 <div class="col-lg-4 col-md-6 col-sm-6">
								<div class="blog-post-box">
									<div class="blog-thumb">
									<a class="featured-services-image" href="<?php the_permalink(); ?>">
										<img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid blog-img" alt="">
									</a>
									</div>
									<h2 class="h6"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
								</div>
							</div>
						<?php

						endwhile;
						endif;
						//wp_reset_query();
					  ?>
					</div>
					<div class="pagination-links  float-right">
								<?php
									$pag_args2 = array(
										'format'  => '?paged2=%#%#blog',
										'current' => $paged2,
										'total'   => $loop_b->max_num_pages,
										'add_args' => array( 'paged1' => $paged1 )
									);
									echo paginate_links( $pag_args2 );
								?>
					</div><!--pagination-->
        </div>
	</div>





</div>

<?php get_footer(); ?>
