<?php
/**
* Template Name: Contact Us
*/

$phone = get_field("phone_number", "options");
$address = get_field("address", "options");
$email = get_field("e-mail", "options");
?>

<?php get_header(); ?>
<?php get_template_part("templates/hero"); ?>
<div class="container breadcrumbs">
  <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
</div>

<div class="container">
  <div class="row">
  <div class="col-md-5">
    <h3 class="block-title">Contact Us</h3>
    <div class="footer-contact contact-info">
      <a href="tel:<?php the_field('phone_number', 'options'); ?>" class="ft-contact-phone"><i data-feather="phone"></i><span><?= $phone; ?></span></a>
      <div class="flex-container">
      <i data-feather="map-pin"></i><p class="ft-contact-address"><span><?= $address; ?></span></p>
      </div>
      <a href="#" class="ft-contact-phone"><i data-feather="mail"></i><span><?= $email; ?></span></a>
    </div><!-- footer-contact -->
  </div>
  <div class="col-md-7">
    <div class="contact-form">
      <h3 class="block-title">Send us a message</h3>
      <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 1, 'title' => false, 'description' => false ) ); ?>
    </div>
  </div>
  </div><!-- row -->
</div>

<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3313.588064492459!2d150.8967481158259!3d-33.84873252545024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b1297025769f66f%3A0x4a67f6ba32de40!2s17%20Elizabeth%20St%2C%20Wetherill%20Park%20NSW%202164!5e0!3m2!1sen!2sau!4v1573609617850!5m2!1sen!2sau" width="100%" height="400" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

<?php get_template_part("content"); ?>
<?php get_footer(); ?>
