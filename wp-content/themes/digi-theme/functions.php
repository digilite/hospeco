<?php
require_once("functions/enqueue.php");
require_once("functions/theme-support.php");
require_once("functions/customize.php");
require_once("functions/acf-fields.php");
require_once("functions/shortcodes.php");
require_once("functions/required-plugins.php");
require_once("functions/custom-post-types.php");
require_once("functions/custom-taxonomies.php");
require_once("functions/reboot.php");

add_action( 'wp_ajax_load_more_posts', 'load_more_posts' );
add_action( 'wp_ajax_nopriv_load_more_posts', 'load_more_posts' );
function load_more_posts(){
    global $post;

    $terms_per_page = 5;
    $offset = isset($_REQUEST["offset"]) ? intval($_REQUEST["offset"]) : 0;
    $taxonomy = $_REQUEST["taxonomy"];
    $product_categories = get_terms(
        [
            'taxonomy' => $taxonomy,
            'number'   => $terms_per_page,
            'offset'   => $offset,
            'parent'   => 0,
        ]
    );
    if( !empty($product_categories) ){ ?>
        <?php foreach ($product_categories as $key => $category) {
            $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $thumbnail_id );
            ?>
            <div class="item">
                <div class="cat-thumb">
                  <a href="<?php echo get_term_link($category); ?>">
                    <?php if($image ) : ?>
                      <img class="cat-thumb-image" src="<?= $image; ?>" alt="">
                      <?php else : ?>
                      <img class="cat-thumb-image" src="<?php bloginfo('url'); ?>/wp-content/uploads/2020/12/empty-300x240-1.jpg" alt="empty-image">
                    <?php endif; ?>
                  </a>
                </div>
                <a class="cat-title" href="<?php echo get_term_link($category); ?>" ><?= $category->name; ?></a>
            </div>
        <?php } ?>
    <?php }
}?>
