<?php $hero_type = get_field_object('hero_type');
$type = $hero_type['value'];
$static_image = get_field("static_image");
$static_title = get_field("static_hero_title");
$static_bt_lable = get_field("static_hero_bt_lable");
$static_bt_url = get_field("static_hero_bt_url");
if($type == 'owl-carousel owl-theme') : $idn = "owl-demo"; endif;

$show_hide = get_field_object('show_hide');
$typee = $show_hide['value'];
?>
<?php if ($typee == 'show') : ?>
<section class="hero">
	<div class="hero-slide <?= $type ?>" id="<?= $idn; ?>">

      <?php
      if($type == 'owl-carousel owl-theme') :
			if( have_rows('hero_slide') ):
    		while( have_rows('hero_slide') ) : the_row();
			   $slideimg = get_sub_field('hero_slide_image');
			   $slidetitle = get_sub_field('hero_slide_title');
			   $slidebutton= get_sub_field('hero_button_lable');
			   $slideurl = get_sub_field('button_url'); ?>
			   <div class="item" class="hero-image-container" style="background: url(<?= $slideimg; ?>) no-repeat center center / cover">
					<div class="container">
						<div class="hero-info">
							<?php if($slidetitle) : ?>
							<h1  class="h1 slide-title"><?= $slidetitle; ?></h1>
							<?php endif; ?>
							<?php if($slideurl) : ?>
							<a href="<?= $slideurl; ?>" class="btn btn-white"><?= $slidebutton; ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
    			<?php endwhile; else : endif; else : ?>
				<div class="item" class="hero-image-container" style="background: url(<?= $static_image; ?>) no-repeat center center / cover">
					<div class="container">
						<div class="hero-info">
							<?php if($static_title) : ?>
							<h1  class="h1 slide-title"><?= $static_title; ?></h1>
							<?php endif; ?>
							<?php if($static_bt_url) : ?>
							<a href="<?= $static_bt_url; ?>" class="btn btn-white"><?= $static_bt_lable; ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
	</div>
  </section>
  <?php endif; ?>
