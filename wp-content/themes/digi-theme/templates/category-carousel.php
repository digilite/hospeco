<?php
$block_title = get_sub_field('pc_title');
$field = get_sub_field_object('slider_type');
$value = $field['value'];
$fsub = get_sub_field_object('subcategory');
$fsubresult = $fsub['value'];
$cat = get_sub_field('choise_parent_category');
$view_all = get_sub_field('view_all_url');
if($value == 'product_cat' && $fsubresult == "yes") {
    $length = count($cat);
}
?>
<section class="category-carousel">
    <div class="container relative-parent">
        <h3 class="block-title"><?= $block_title; ?></h3>
        <a href="<?= esc_url( $view_all ); ?>" class="view-all-cat">View all</a>
    <?php if($length > 1) : ?>
        <?php if( $cat ): ?>
            <div class="category-carousel-slide owl-carousel owl-theme">
                <?php foreach( $cat as $term ):
                    $tr = get_term( $term );
                    $thumbnail_id1 = get_woocommerce_term_meta( $tr->term_id, 'thumbnail_id', true );
                    $image1 = wp_get_attachment_url( $thumbnail_id1 ); ?>
                    <div class="item">
                        <div class="cat-thumb">
                            <?php if($image1) : ?>
                            <a href="<?php echo get_term_link($term); ?>">
                                <img class="cat-thumb-image" src="<?= $image1; ?>" alt="">
                            </a>
                            <?php else : ?>
                            <a href="<?php echo get_term_link($term); ?>">
                                <img class="cat-thumb-image" src="https://dev.digilabs.ca/hospeco/wp-content/uploads/2016/12/no_image-100x100.jpg" alt="">
                            </a>
                            <?php endif; ?>
                        </div>
                        <a class="cat-title" href="<?php echo get_term_link($term); ?>" ><?= $tr->name; ?></a>
                    </div>
                    <?php endforeach; ?>
            </div>
            <?php endif;
            else:
            $parent = $cat[0];?>
            <?php
            $children = get_categories( array(
                'child_of'      => $parent,
                'taxonomy'      => 'product_cat',
                'hide_empty'    => false,
                'fields'        => 'ids',
            ) );
            if(count($children) > 1) : $parent = $cat[0]; else : $parent = 0; endif;
            $cat_args = array(
                'parent' => $parent
            );

            $product_categories = get_terms( $value, $cat_args );
            if( !empty($product_categories) ) : ?>
            <div class="category-carousel-slide owl-carousel owl-theme">
                <?php foreach ($product_categories as $key => $category) {
                    $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                    $image = wp_get_attachment_url( $thumbnail_id );
                    ?>
                    <div class="item">
                        <div class="cat-thumb">
                            <?php if($image ) : ?>
                            <a href="<?php echo get_term_link($category); ?>">
                                <img class="cat-thumb-image" src="<?= $image; ?>" alt="category image">
                            </a>
                            <?php else : ?>
                            <a href="<?php echo get_term_link($category); ?>">
                                <img class="cat-thumb-image" src="<?= site_url('/wp-content/uploads/2020/12/empty-300x240-1.jpg'); ?>" alt="category image">
                            </a>
                            <?php endif; ?>
                        </div>
                        <a class="cat-title" href="<?php echo get_term_link($category); ?>" ><?= $category->name; ?></a>
                    </div>
                <?php } ?>
            </div>
            <?php endif; ?>
    <?php endif; ?>
    </div>
</section>


