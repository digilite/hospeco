<?php
/*
Template shows the latest blog post
*/

$args = array(
  'post_type' => 'post',
  'posts_per_page' => 1,
);
?>
<section class="latest-post">
<div class="container">
<?php
$post_query = new WP_Query($args);
if ($post_query->have_posts()) : ?>
<div class="row">
<?php
while ($post_query->have_posts()) :
       $post_query->the_post(); ?>
<div class="bg-text-over">
  <div class="container">
      <div class="relative-parent">
          <div class="the-bg bg-styles" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
          <div class="the-bg-content">
              <h3><?php the_title() ?></h3>
              <p><?php echo wp_trim_words( get_the_content(), 59, '...' ); ?></p>
              <a class="read-more-link" href="<?php the_permalink(); ?>">Read More</a>
          </div><!-- the-bg-content -->
      </div>
  </div>
</div>
<?php endwhile;
endif;
wp_reset_postdata();
?>
</div>
</div>
</section>
