<section class="popular-products">
<?php $s2_title = get_sub_field('popular_products_title'); ?>
<div class="container relative-parent">
<h3 class="block-title"><?= $s2_title; ?></h3>
<div class="owl-carousel owl-theme" id="product-carousel">
<?php
$popular_products = get_sub_field('popular_products');
if( $popular_products ): ?>
    <?php foreach( $popular_products as $post ):
        // Setup this post for WP functions (variable must be named $post).
        setup_postdata($post); ?>
        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' ); ?>
        <div class="item product-box">
            <a class="product-img" href="<?php the_permalink(); ?>">
              <?php if($image ) : ?>
                <img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
              <?php else : ?>
                <img class="cat-thumb-image" src="<?php bloginfo('url'); ?>/wp-content/uploads/2020/12/empty-300x240-1.jpg" data-id="<?php echo $loop->post->ID; ?>" alt="empty-image">
              <?php endif; ?>
            </a>
            <div class="summary entry-summary">
                <?php woocommerce_template_single_price(); ?>
                <h3 class="h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <?php echo do_shortcode('[add_to_cart id="'.$post->ID.'" show_price="false"]'); ?>
            </div>
        </div><!-- Products Box -->
    <?php endforeach; ?>
    <?php
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
<?php endif; ?>
</div>
</div>
</section>
