<?php 
$block_title = get_sub_field('hp_shortcode');
?>
<section class="tracking">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="tracking-bl-title">
                <p><i class="fas fa-map-marked-alt"></i><?= $block_title; ?></p>
            </div>
            <?php echo do_shortcode('[dfe-tracking]'); ?>
        </div>
    </div>
</section>


