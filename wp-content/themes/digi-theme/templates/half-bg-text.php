<?php
/*
Template for half text half background.
*/

$background = get_sub_field("backgroud_image");
$title = get_sub_field("the_title");
$description = get_sub_field("description");
?>

<section class="hf-text-bg">
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <div class="hf-text-content">
                <h3 class="block-title desktop-menu"><?= $title ?></h3>
                <div class="editor-styles"><?= $description ?></div>
            </div>
        </div>
        <div class="col-md-5">
            <h3 class="block-title mobile-menu"><?= $title ?></h3>
            <div class="bg-styles" style="background-image: url(<?= $background ?>);"></div>
        </div>
    </div>
</div>
</section>
