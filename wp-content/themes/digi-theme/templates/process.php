<?php
/*
Template for the process in blue
*/

$first = get_sub_field("first");
$second = get_sub_field("second");
$third = get_sub_field("third");
$fourth = get_sub_field("fourth");
?>

<section class="the-process">
<div class="container">
  <div class="process-content d-flex justify-content-around align-items-center">
    <span><?= $first ?></span>
    <span class="process-icon"></span>
    <span><?= $second ?></span>
    <span class="process-icon"></span>
    <span><?= $third ?></span>
    <span class="process-icon"></span>
    <span><?= $fourth ?></span>
  </div>
</div>
</section>
