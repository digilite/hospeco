<?php
/* 
Template for why to choose Hospeco section.
*/

$background_url = get_sub_field("why_choose_bg");
?>

<section class="why-hospeco">
<div class="container">
<div class="bg-styles" style="background-image: url(<?= $background_url ?>); min-height: 230px;"></div>
<?php
if( have_rows("why_choose_hospeco") ): ?>
    <div class="row why-choose-list">
    <?php
    while ( have_rows("why_choose_hospeco") ) : the_row(); 
    $icon = get_sub_field("icon");
    $why_hospeco = get_sub_field("why_text");
    ?>
        <div class="col-md-6">
            <div class="why-content d-flex align-items-center">
                <div class="bg-styles" style="background-image: url(<?= $icon ?>);"></div>
                <p><?= $why_hospeco ?></p>
            </div>
        </div>
    <?php endwhile; ?>
    </div><!--row-->
<?php endif; ?>
</div>
</section>