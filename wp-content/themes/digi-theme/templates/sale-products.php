<section class="on-sale">
<?php $s_title = get_sub_field('sale_products_title'); ?>
<div class="container relative-parent">
<h3 class="block-title"><?= $s_title; ?></h3>
<a href="<?= esc_url( get_page_link( 19528 ) ); ?>" class="view-all-cat">View all</a>
<?php $query_args = array(
    'posts_per_page'    => -1,
    'no_found_rows'     => 1,
    'post_status'       => 'publish',
    'post_type'         => 'product',
    'meta_query'        => WC()->query->get_meta_query(),
    'post__in'          => array_merge( array( 0 ), wc_get_product_ids_on_sale() )
);
$products = new WP_Query( $query_args );
if ($products->have_posts()) : ?>
<div class="owl-carousel owl-theme" id="product_carousel">
<?php
while ($products->have_posts()) :
    $products->the_post();
    ?>

    <div class="item product-box">
        <span class="item-is-on">Sale</span>
        <a class="product-img" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
        <div class="summary entry-summary">
            <?php woocommerce_template_single_price(); ?>
            <h3 class="h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <?php echo do_shortcode('[add_to_cart id="'.$post->ID.'" show_price="false"]'); ?>
        </div>
    </div>
<?php endwhile; ?>
</div>
<?php
endif;
wp_reset_postdata();
?>
</div>
</section>


