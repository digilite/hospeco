<section class="popular-products">
<?php $s2_title = get_sub_field('popular_products_title'); ?>
<div class="container">
<h3 class="block-title"><?= $s2_title; ?></h3>
<div class="owl-carousel owl-theme prod-carousel">
<?php $cat = get_sub_field('choise_category');
  $args = array(
    'post_type' => 'product',
    'posts_per_page' => 5,
    'tax_query' => array(
      array(
        'taxonomy' => 'product_cat',
        'terms' =>$cat
      ),
    ),
  );
  $quote_query = new WP_Query($args);
  if ($quote_query->have_posts()) {
    while($quote_query->have_posts()) {
      $quote_query->the_post();
      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' ); ?>
      <div class="item product-box">
            <a class="product-img" href="<?php the_permalink(); ?>"><img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>"></a>
            <div class="summary entry-summary">
                <?php woocommerce_template_single_price(); ?>
                <h3 class="h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <?php echo do_shortcode('[add_to_cart id="'.$post->ID.'" show_price="false"]'); ?>
            </div>
        </div>
    <?php }
  }
  wp_reset_postdata();
 ?>
</div>
</div>
</section>
