<?php
/**
* Template Name: About Us
*/ ?>

<?php get_header(); ?>
<?php get_template_part("templates/hero"); ?>
<div class="container breadcrumbs">
  <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
</div>
<?php get_template_part("content"); ?>
<?php get_footer(); ?>
