<?php
/**
* Template Name: Shop page
*/ ?>
<?php get_header(); ?>

<?php get_template_part("templates/hero"); ?>
<section class="categories">
    <div class="container">
    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
    <?php
        $taxanomy = 'product_cat';
        $terms_per_page = 20;
        $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $offset       = ( $terms_per_page * $current_page ) - $terms_per_page;
        $product_categories = get_terms('product_cat',
            [
                'number'   => $terms_per_page,
                'offset'   => $offset,
                'paged'    => $current_page,
                'parent'   => 0
            ]
        );
        if( !empty($product_categories) ){ ?>
        <div class="row justify-content-between" id="">
            <?php foreach ($product_categories as $key => $category) {
                $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                $image = wp_get_attachment_url( $thumbnail_id );
                ?>
                <div class="item">
                    <div class="cat-thumb">
                        <?php if($image ) : ?>
                        <a href="<?php echo get_term_link($category); ?>">
                            <img class="cat-thumb-image" src="<?= $image; ?>" alt="">
                        </a>
                        <?php else : ?>
                        <a href="<?php echo get_term_link($category); ?>">
                            <img class="cat-thumb-image" src="http://localhost/hospeco/wp-content/uploads/2021/07/" alt="">
                        </a>
                        <?php endif; ?>
                    </div>
                    <a class="cat-title" href="<?php echo get_term_link($category); ?>" ><?= $category->name; ?></a>
                </div>
            <?php } ?>
        </div>
        <?php } ?>
<p id="load_more_category" data-taxonomy="product_cat">Load more</p>
    </div>
</section>
<?php get_template_part("content"); ?>

<?php get_footer(); ?>
