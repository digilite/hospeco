<?php
 if (is_shop() ||  is_product_category()) {
    if( have_rows("page_content", "19439") ):
        // loop through the rows of data
        while ( have_rows("page_content", "19439") ) : the_row();
            switch (get_row_layout()) :
                case "product_category":
                    get_template_part("templates/category-carousel");
                    break;
                case "shortcode":
                    get_template_part("templates/shortcode");
                    break;
                case "sale_products":
                    get_template_part("templates/sale-products");
                    break;
                case "popular_products":
                    get_template_part("templates/popular-products");
                     break;
                case "products_by_category":
                    get_template_part("templates/products-by-category");
                    break;
                case "popular_products":
                    get_template_part("templates/popular-products");
                     break;
                case "show_latest_post":
                    get_template_part("templates/last-blog-post");
                    break;
                case "hf_bg_text":
                    get_template_part("templates/half-bg-text");
                    break;
                case "why_hospeco":
                    get_template_part("templates/why-hospeco");
                    break;
                case "the_process":
                  get_template_part("templates/process");
                  break;
            endswitch;
        endwhile;
    endif;
} else {
  if( have_rows("page_content") ):
    // loop through the rows of data
    while ( have_rows("page_content") ) : the_row();
        switch (get_row_layout()) :
            case "product_category":
                get_template_part("templates/category-carousel");
                break;
            case "shortcode":
                get_template_part("templates/shortcode");
                break;
            case "sale_products":
                get_template_part("templates/sale-products");
                break;
            case "popular_products":
                get_template_part("templates/popular-products");
                 break;
            case "products_by_category":
                get_template_part("templates/products-by-category");
                break;
            case "popular_products":
                get_template_part("templates/popular-products");
                 break;
            case "show_latest_post":
                get_template_part("templates/last-blog-post");
                break;
            case "hf_bg_text":
                get_template_part("templates/half-bg-text");
                break;
            case "why_hospeco":
                get_template_part("templates/why-hospeco");
                break;
            case "the_process":
              get_template_part("templates/process");
              break;
        endswitch;
    endwhile;
endif;
}
?>

