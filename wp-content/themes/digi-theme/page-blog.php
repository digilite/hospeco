<?php
/**
* Template Name: Blog
*/
?>

<?php get_header(); ?>
<?php get_template_part("templates/hero"); ?>
<div class="container">
  <div class="breadcrumbs">
    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
  </div>
  <div class="blog-posts">
    <?php
    $current = get_query_var('paged');
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 13,
        'paged' => $current
    );

    $post_query = new WP_Query($args);
	if ($post_query->have_posts()) : ?>
    <div class="row">
      <?php
      while ($post_query->have_posts()) :
             $post_query->the_post();
      if ( $post_query->current_post == 0 ) { ?>
      <div class="bg-text-over hero-blog">
        <div class="container">
            <div class="relative-parent">
                <div class="the-bg bg-styles" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                <div class="the-bg-content">
                    <h3><?php the_title() ?></h3>
                    <p><?php echo wp_trim_words( get_the_content(), 59, '...' ); ?></p>
                    <a class="read-more-link" href="<?php the_permalink(); ?>">Read More</a>
                </div><!-- the-bg-content -->
            </div>
        </div>
        </div>
      <?php } else { ?>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="blog-post-box">
          <div class="blog-thumb">
            <a class="img-link" href="<?php the_permalink(); ?>">
            <?php
            if ( has_post_thumbnail() ) { ?>
              <img src="<?php the_post_thumbnail_url() ?>" class="img-fluid blog-img" alt="blog image">
            <?php
            } else { ?>
              <img src="<?= site_url('/wp-content/themes/digi-theme/img/logo.svg') ?>" class="img-fluid blog-img blog-back-image" alt="blog image">
            <?php } ?>
            </a>
          </div>
          <h2 class="h5"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
        </div>
      </div>
      <?php }
      endwhile; ?>

      <div class="pagination-links">
        <?php
        echo paginate_links(array(
            "total" => $post_query->max_num_pages,
        ));
        ?>
      </div>
      <!--pagination-->
    </div>
    <!--ROW-->
    <?php endif;

    wp_reset_postdata();?>
  </div>
</div>
<?php get_template_part("content"); ?>
<?php get_footer(); ?>
