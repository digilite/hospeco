<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
$heroimage = get_field('hero_image', 'options');
$herotitle = get_field('hero_title', 'options');
$bt_lable = get_field('hero_bt_lable', 'options');
$bt_url = get_field('hero_bt_url', 'options');
?>
<section class="hero">
<div class="hero-slide">
  <div class="item" class="hero-image-container" style="background: url(<?= $heroimage; ?>) no-repeat center center / cover">
    <div class="container">
      <div class="hero-info">
        <?php if($herotitle) : ?>
        <h1 class="h1 slide-title"><?= $herotitle; ?></h1>
        <?php endif; ?>
        <?php if($bt_lable) : ?>
        <a href="<?= $bt_url; ?>" class="btn btn-white"><?= $bt_lable; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div><!-- Item -->
</div><!-- Hero Slide -->
</section>
<?php
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */ ?>

<div class="container">
<header class="woocommerce-products-header">

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<section class="categories">
    <div class="container">
    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
    <?php
        $taxanomy = 'product_cat';
        $terms_per_page = 20;
        $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $offset       = ( $terms_per_page * $current_page ) - $terms_per_page;
        $product_categories = get_terms('product_cat',
            [
              'number'   => $terms_per_page,
              'offset'   => $offset,
              'paged'    => $current_page,
              'parent'   => 0
            ]
        );
        if( !empty($product_categories) ){ ?>
        <div class="row justify-content-between" id="">
            <?php foreach ($product_categories as $key => $category) {
                $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                $image = wp_get_attachment_url( $thumbnail_id );
                ?>
                <div class="col-6 item">
                    <div class="cat-thumb">
                        <?php if($image ) : ?>
                        <a href="<?php echo get_term_link($category); ?>">
                            <img class="cat-thumb-image" src="<?= $image; ?>" alt="">
                        </a>
                        <?php else : ?>
                        <a href="<?php echo get_term_link($category); ?>">
                            <img class="cat-thumb-image" src="<?php bloginfo('url'); ?>/wp-content/uploads/2020/12/empty-300x240-1.jpg" alt="">
                        </a>
                        <?php endif; ?>
                    </div>
                    <a class="cat-title" href="<?php echo get_term_link($category); ?>" ><?= $category->name; ?></a>
                </div>
            <?php } ?>
        </div>
        <?php } ?>
<p id="load_more_category" data-taxonomy="product_cat">Load more</p>
    </div>
</section>

<?php do_action( 'woocommerce_after_main_content' ); ?>

</div>
<?php get_template_part("content"); ?>

<?php get_footer( 'shop' ); ?>

