<?php
/**
* Template Name: Sale
*/
?>

<?php get_header(); ?>

<div class="container sale-page">
  <div class="breadcrumbs">
    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
  </div>
  <div class="row">
    <div class="col-lg-3 col-md-4 col-sm-12">
      <?php do_action( 'woocommerce_sidebar' ); ?>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-12">
      <h3 class="block-title">Sale Products</h3>
      <?php echo do_shortcode('[products columns="4" per_page="20" paginate="1" orderby="popularity" on_sale="true" ]'); ?>
    </div>
  </div><!-- row -->
</div>
<?php get_template_part("content"); ?>

<?php get_footer(); ?>
